import matplotlib
matplotlib.use('Agg')
import click

import numpy as np
import cv2



def detect_chessboard(img, pattern_size=(11, 7), square_size=1.0):
    pattern_points = np.zeros((np.prod(pattern_size), 3), np.float32)
    pattern_points[:, :2] = np.indices(pattern_size).T.reshape(-1, 2)
    # TODO: trace it
    pattern_points *= square_size

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    h, w = gray.shape[:2]
    found, corners = cv2.findChessboardCorners(gray, pattern_size)
    if found:
        term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1)
        cv2.cornerSubPix(gray, corners, (5, 5), (-1, -1), term)
    vis = img.copy()
    cv2.drawChessboardCorners(vis, pattern_size, corners, found)
    return found, vis


def main():
    cam = cv2.VideoCapture(0)

    pairs = []
    counter = 16
    while True:
        cam.grab()
        _, frame = cam.retrieve()

        cv2.imshow('res', cv2.resize(frame, (320, 240)))

        f, v = detect_chessboard(cv2.resize(frame, (640, 480)))

        cv2.imshow('board', v)

        d = 100

        if f:
            d = 0
        key = cv2.waitKey(d)
        if key == ord('q'):
            break


        if f and key == ord('s'):
            print 'write images ', counter
            cv2.imwrite('c615/chess_board_{:02}.jpg'.format(counter), frame)
            counter = counter + 1

    cam.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()