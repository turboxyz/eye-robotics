#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"


using namespace std;
using namespace cv;

int main() {

    VideoCapture cap(0);

    if(!cap.isOpened()) {
        cout << "Cannot open webcam" << endl;
        return -1;
    }

    namedWindow("Control", CV_WINDOW_AUTOSIZE);

    int lowH = 0, highH =179;
    int lowS = 0, highS = 255;
    int lowV = 0, highV = 255;

    cvCreateTrackbar("lowH", "Control", &lowH, 179);
    cvCreateTrackbar("highH", "Control", &highH, 179);
    
    cvCreateTrackbar("lowS", "Control", &lowS, 255);
    cvCreateTrackbar("highS", "Control", &highS, 255);

    cvCreateTrackbar("lowV", "Control", &lowV, 255);
    cvCreateTrackbar("highV", "Control", &highV, 255);


    Mat frame, hsv, thr;
    while(1) {

        bool ok = cap.read(frame);

        if(!ok) {
            cout << "Cannot read" << endl;
            break;
        }

        cvtColor(frame, hsv, COLOR_BGR2HSV);
        
        inRange(hsv, Scalar(lowH, lowS, lowV), Scalar(highH, highS, highV), thr);

        erode(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
        dilate(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
        
        dilate(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
        erode(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));


        imshow("input", frame);
        imshow("selected", thr);

        if(waitKey(25) == 27) {
            break;
        }
    }

    destroyAllWindows();

    return 0;
}

