from flask import Flask, request

app = Flask(__name__)
# just dirty fast solution, last data is a global static.
last_data = ""

@app.route('/', methods=['GET', 'POST'])
def index():
    global last_data
    if request.method == 'POST':
        last_data = request.data
    return last_data, 200

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
