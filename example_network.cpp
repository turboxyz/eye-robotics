#include <iostream>

#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/opencv.hpp>

#include "restclient-cpp/restclient.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

using namespace std;
using namespace cv;


namespace {
    const char* about = "http sender";
    const char* keys = 
        "{ip  | 127.0.0.1 | receiver ip adress }";
}


int main(int argc, char *argv[]) {
    
    String ip;
    
    CommandLineParser parser(argc, argv, keys);
    parser.about(about);

    if(argc < 2) {
        parser.printMessage();
        return 0;
    }

    ip = parser.get<String>("ip");
    cout << ip << endl;

    return 0;
}


