/*
By downloading, copying, installing or using the software you agree to this
license. If you do not agree to this license, do not download, install,
copy or use the software.

                          License Agreement
               For Open Source Computer Vision Library
                       (3-clause BSD License)

Copyright (C) 2013, OpenCV Foundation, all rights reserved.
Third party copyrights are property of their respective owners.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

  * Neither the names of the copyright holders nor the names of the contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

This software is provided by the copyright holders and contributors "as is" and
any express or implied warranties, including, but not limited to, the implied
warranties of merchantability and fitness for a particular purpose are
disclaimed. In no event shall copyright holders or contributors be liable for
any direct, indirect, incidental, special, exemplary, or consequential damages
(including, but not limited to, procurement of substitute goods or services;
loss of use, data, or profits; or business interruption) however caused
and on any theory of liability, whether in contract, strict liability,
or tort (including negligence or otherwise) arising in any way out of
the use of this software, even if advised of the possibility of such damage.
*/


#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>

#include <opencv2/opencv.hpp>

#include "restclient-cpp/restclient.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"


#include <iostream>

using namespace std;
using namespace cv;


#define WIDTH 300
#define HEIGHT 300

namespace {
    const char* about = "Basic marker detection";
    const char* keys  =
            "{d        |       | dictionary: DICT_4X4_50=0, DICT_4X4_100=1, DICT_4X4_250=2,"
                    "DICT_4X4_1000=3, DICT_5X5_50=4, DICT_5X5_100=5, DICT_5X5_250=6, DICT_5X5_1000=7, "
                    "DICT_6X6_50=8, DICT_6X6_100=9, DICT_6X6_250=10, DICT_6X6_1000=11, DICT_7X7_50=12,"
                    "DICT_7X7_100=13, DICT_7X7_250=14, DICT_7X7_1000=15, DICT_ARUCO_ORIGINAL = 16}"
                    "{v        |       | Input from video file, if ommited, input comes from camera }"
                    "{ci       | 0     | Camera id if input doesnt come from video (-v) }"
                    "{c        |       | Camera intrinsic parameters. Needed for camera pose }"
                    "{l        | 0.1   | Marker side lenght (in meters). Needed for correct scale in camera pose }"
                    "{dp       |       | File of marker detector parameters }"
                    "{r        |       | show rejected candidates too }"
        		    "{D        |       | show debug ouput }"
                    "{ip  | 127.0.0.1:5000 | receiver address:port }";
}

/**
 */
static bool readCameraParameters(string filename, Mat &camMatrix, Mat &distCoeffs) {
    FileStorage fs(filename, FileStorage::READ);
    if(!fs.isOpened())
        return false;
    fs["camera_matrix"] >> camMatrix;
    fs["distortion_coefficients"] >> distCoeffs;
    return true;
}



/**
 */
static bool readDetectorParameters(string filename, aruco::DetectorParameters &params) {
    FileStorage fs(filename, FileStorage::READ);
    if(!fs.isOpened())
        return false;
    fs["adaptiveThreshWinSizeMin"] >> params.adaptiveThreshWinSizeMin;
    fs["adaptiveThreshWinSizeMax"] >> params.adaptiveThreshWinSizeMax;
    fs["adaptiveThreshWinSizeStep"] >> params.adaptiveThreshWinSizeStep;
    fs["adaptiveThreshConstant"] >> params.adaptiveThreshConstant;
    fs["minMarkerPerimeterRate"] >> params.minMarkerPerimeterRate;
    fs["maxMarkerPerimeterRate"] >> params.maxMarkerPerimeterRate;
    fs["polygonalApproxAccuracyRate"] >> params.polygonalApproxAccuracyRate;
    fs["minCornerDistanceRate"] >> params.minCornerDistanceRate;
    fs["minDistanceToBorder"] >> params.minDistanceToBorder;
    fs["minMarkerDistanceRate"] >> params.minMarkerDistanceRate;
    fs["doCornerRefinement"] >> params.doCornerRefinement;
    fs["cornerRefinementWinSize"] >> params.cornerRefinementWinSize;
    fs["cornerRefinementMaxIterations"] >> params.cornerRefinementMaxIterations;
    fs["cornerRefinementMinAccuracy"] >> params.cornerRefinementMinAccuracy;
    fs["markerBorderBits"] >> params.markerBorderBits;
    fs["perspectiveRemovePixelPerCell"] >> params.perspectiveRemovePixelPerCell;
    fs["perspectiveRemoveIgnoredMarginPerCell"] >> params.perspectiveRemoveIgnoredMarginPerCell;
    fs["maxErroneousBitsInBorderRate"] >> params.maxErroneousBitsInBorderRate;
    fs["minOtsuStdDev"] >> params.minOtsuStdDev;
    fs["errorCorrectionRate"] >> params.errorCorrectionRate;
    return true;
}



/**
 */
int main(int argc, char *argv[]) {
    RestClient::init();

    rapidjson::StringBuffer s;


    CommandLineParser parser(argc, argv, keys);
    parser.about(about);

    if(argc < 2) {
        parser.printMessage();
        return 0;
    }

    int dictionaryId = parser.get<int>("d");
    bool showRejected = parser.has("r");
    bool showDebug = parser.has("D");
    bool estimatePose = parser.has("c");
    float markerLength = parser.get<float>("l");

    String ip = parser.get<String>("ip");

    Ptr<aruco::DetectorParameters> detectorParams = aruco::DetectorParameters::create();
    if(parser.has("dp")) {
        bool readOk = readDetectorParameters(parser.get<string>("dp"), *detectorParams);
        if(!readOk) {
            cerr << "Invalid detector parameters file" << endl;
            return 0;
        }
    }
    detectorParams->doCornerRefinement = true; // do corner refinement in markers

    int camId = parser.get<int>("ci");

    String video;
    if(parser.has("v")) {
        video = parser.get<String>("v");
    }

    if(!parser.check()) {
        parser.printErrors();
        return 0;
    }

    Ptr<aruco::Dictionary> dictionary =
            aruco::getPredefinedDictionary(aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

    Mat camMatrix, distCoeffs;
    if(estimatePose) {
        bool readOk = readCameraParameters(parser.get<string>("c"), camMatrix, distCoeffs);
        if(!readOk) {
            cerr << "Invalid camera file" << endl;
            return 0;
        }
    }

    VideoCapture inputVideo;
    int waitTime;
    if(!video.empty()) {
        inputVideo.open(video);
        waitTime = 0;
    } else {
        inputVideo.open(camId);
        waitTime = 10;
    }

    double totalTime = 0;
    int totalIterations = 0;
    unsigned int i;

    vector< int > ids;
    vector< vector< Point2f > > corners, rejected;
    vector< Vec3d > rvecs, tvecs;

    vector<int> scene_ids;
    vector<Point2f> image_coords;
    vector<Point2f> scene_coords;
    vector<Point2f> image_double_coords;
    vector<Point2f> scene_double_coords;
    string buf; 
    vector<double> scene_orientations;
    
    vector<Point2f> image_center_coords;
    vector<Point2f> scene_center_coords;

    Point2f scene_corners [4];

    Scalar lowX(86, 98, 162), highX(131, 178, 255);
    int canny_low = 100, canny_high = 200;

    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    Mat canny, thr, hsv;

/*  
    // screen-like order of points
    // top-left = (0, 0), bottom-right=(300, 300)

    Point2f inscene_corners [] = {Point2f(0,300),
				  Point2f(300, 300),
				  Point2f(300, 0),
				  Point2f(0, 0)};
*/
    // human-friendly order of points
    // bottom-left = (0, 0) top-right=(300,300)
    Point2f inscene_corners [] = {
                  Point2f(0,0),
				  Point2f(300, 0),
				  Point2f(300, 300),
				  Point2f(0, 300)
    };

    Mat image, imageCopy, warped(300, 300, CV_32FC1), dwarped(300, 300, CV_32FC1);
    Mat M;
    int cornersDetected = 0;

    bool readyTransform = false;
	
    while(inputVideo.grab()) {
        inputVideo.retrieve(image);

        double tick = (double)getTickCount();

        // detect markers and estimate pose
        aruco::detectMarkers(image, dictionary, corners, ids, detectorParams, rejected);
        if(estimatePose && ids.size() > 0)
            aruco::estimatePoseSingleMarkers(corners, markerLength, camMatrix, distCoeffs, rvecs,
                                             tvecs);

        double currentTime = ((double)getTickCount() - tick) / getTickFrequency();

        totalTime += currentTime;
        totalIterations++;


    	if((totalIterations % 30 == 0) || (!readyTransform)) {
	        cornersDetected = 0;
	        for(i = 0; i < ids.size(); ++i) {
        		if(ids[i] <= 3)
	        	    ++cornersDetected;
	        }
            if(cornersDetected == 4) {
		        readyTransform = false;
        		for(i = 0; i < ids.size(); ++i) {
		            if(ids[i] <= 3)
        			scene_corners[ids[i]] = corners[i][0];
		        }
        		M = getPerspectiveTransform(scene_corners, inscene_corners);
	            readyTransform = true;
            }
    	}


        if(totalIterations % 30 == 0) {
            cout << "Detection Time = " << currentTime * 1000 << " ms "
            << "(Mean = " << 1000 * totalTime / double(totalIterations) << " ms)" << endl;
            cout << "Points detected: \n";
            for(i = 0; i < ids.size(); ++i) {
                cout << ids[i] << " : " << corners[i][0] << " : " << corners[i][1] << endl;
            }
            cout << "---------------------------" << endl;
        }

        // draw results
        image.copyTo(imageCopy);
        if(ids.size() > 0) {
            aruco::drawDetectedMarkers(imageCopy, corners, ids);

            if(estimatePose) {
                for(unsigned int i = 0; i < ids.size(); i++)
                    aruco::drawAxis(imageCopy, camMatrix, distCoeffs, rvecs[i], tvecs[i],
                                    markerLength * 0.5f);
            }
        }

	
    	if(readyTransform) {
            scene_coords.clear();
            image_coords.clear();
            scene_double_coords.clear();
            image_double_coords.clear();
            scene_orientations.clear();
            image_center_coords.clear();
            scene_center_coords.clear();
            scene_ids.clear();
            for(i = 0; i < ids.size(); ++i) {
    	        if(ids[i] <= 3)
                    continue;
        	    image_coords.push_back(corners[i][0]);
                image_double_coords.push_back(corners[i][0]);
                image_double_coords.push_back(corners[i][1]);
                double x = 0.0, y = 0.0;
                for(int j = 0; j < 4; ++j) {
                    x += 0.25 * corners[i][j].x;
                    y += 0.25 * corners[i][j].y;
                }
                image_center_coords.push_back(Point2f(x, y));
                scene_ids.push_back(ids[i]);
            }
            if(image_coords.size() > 0) {
                perspectiveTransform(image_coords, scene_coords, M);
                perspectiveTransform(image_double_coords, scene_double_coords, M);	
                perspectiveTransform(image_center_coords, scene_center_coords, M);
            }
            for(i = 0; i < image_coords.size(); ++i) {
                double x, y;
                x = scene_double_coords[2*i + 1].x - scene_double_coords[2 * i].x;
            	y = scene_double_coords[2*i + 1].y - scene_double_coords[2 * i].y;
        		scene_orientations.push_back(atan2(y, x));
	        }
            // Let's detect obstacles
	        warpPerspective(imageCopy, warped, M, warped.size()); 
            
            cvtColor(warped, hsv, COLOR_BGR2HSV);
            inRange(hsv, lowX, highX, thr);

            erode(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
            dilate(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

            dilate(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
            erode(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

            blur(thr, thr, Size(3, 3));

            Canny(thr, canny, canny_low, canny_high, 3);
            findContours(canny, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));
	    }
	

        // send results
    	if(scene_coords.size() > 0) {
	        s.Clear();
            rapidjson::Writer<rapidjson::StringBuffer> writer(s);
            writer.StartObject();               // Between StartObject()/EndObject(),
            writer.Key("objects");                // output a key,
            writer.StartArray();
            for(i = 0; i < scene_coords.size(); ++i) {
                writer.StartObject();
                   writer.Key("x");
                       writer.Double((scene_center_coords[i].x));
                   writer.Key("y");
                       writer.Double((scene_center_coords[i].y));
                   writer.Key("id");
                       writer.Int(scene_ids[i]);
  	               writer.Key("phi");
                       writer.Double(scene_orientations[i]);
                writer.EndObject();
            }
            writer.EndArray();
            if(contours.size() > 0) {
                writer.Key("obstacle");
		writer.StartObject();
                // send only first contour
                writer.Key("x");
                writer.StartArray();
                for(i = 0; i < contours[0].size(); ++i) {
                    writer.Double((contours[0][i].x));
                }
                writer.EndArray();
                writer.Key("y");
                writer.StartArray();
                for(i = 0; i < contours[0].size(); ++i) {
                    writer.Double((contours[0][i].y));
                }
                writer.EndArray();
		writer.EndObject();
            }
            writer.EndObject();
            RestClient::Response r = RestClient::post(ip, "text/json", s.GetString());

	    }

        // debug output
        //
        if(showDebug) {
            imshow("out", imageCopy);
        }

     	if(showDebug && readyTransform) {	
//            imshow("out", imageCopy);

	        warpPerspective(imageCopy, warped, M, warped.size()); 
  	        // we need wrap for scene and flip for displaying

    	    Scalar c = Scalar(3, 120, 255);
	        for(i = 0; i < scene_center_coords.size(); ++i) {
	        	circle(warped, scene_center_coords[i], 3, c);
	        }
    	    flip(warped, dwarped, 0);

	        imshow("warped", dwarped);
    	}

        char key = (char)waitKey(waitTime);
        if(key == 27) break;
    }

    RestClient::disable();

    return 0;
}
