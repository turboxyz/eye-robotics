#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"


using namespace std;
using namespace cv;

RNG rng(42);

int main() {

    VideoCapture cap(0);

    if(!cap.isOpened()) {
        cout << "Cannot open webcam" << endl;
        return -1;
    }


    Scalar lowRed(0, 40, 143);
    Scalar highRed(33, 255, 153);

    Scalar lowBlue(83, 85, 54);
    Scalar highBlue(105, 255, 112);

    Scalar lowGreen(77, 40, 114);
    Scalar highGreen(97, 255, 153);

    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;


    Mat frame, hsv, thr, canny;
    while(1) {

        bool ok = cap.read(frame);

        if(!ok) {
            cout << "Cannot read" << endl;
            break;
        }

        cvtColor(frame, hsv, COLOR_BGR2HSV);
        
        inRange(hsv, lowBlue, highBlue, thr);

        erode(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
        dilate(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
        
        dilate(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
        erode(thr, thr, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));


        imshow("input", frame);
        imshow("selected", thr);

        blur(thr, thr, Size(3, 3));

        Canny(thr, canny, 100, 200, 3);
        findContours(canny, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));

        for(int i = 0; i < contours.size(); ++i) {
            Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
            drawContours(frame, contours, i, color, 2, 8, hierarchy, 0, Point());
        }
        
        if(contours.size() > 0) {
            for(int i = 0; i < contours[0].size(); ++i) {
                cout << contours[0][i].x << contours[0][i].y << endl;
            }
        }

        imshow("Contours", frame);

        if(waitKey(25) == 27) {
            break;
        }
    }

    destroyAllWindows();

    return 0;
}

